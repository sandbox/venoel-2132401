CONTENTS OF THIS FILE
=====================

 * Introduction
 * Information
 * Dependencies
 * Installation
 * Configuration
    - FancyBox module
    - Format Text
 * Using
 * Support

INTRODUCTION
============

How make gallery from images in node body? Use fancyBox 
and addgalleryclass_filter!

Current Maintainer for 7.x-2.x: Denis Olenev <denis.olenev@gmail.com>

The addgalleryclass_filter allow add attribute to <a> tag to organise 
image gallery with fancyBox

INFORMATION
===========

This module only supports fancyBox version 2.1.0 and higher. 

DEPENDENCIES
============

None. If fancyBox not installed you just add excess atribute and all.

INSTALLATION
============

1. Install and enable the Libraries API and the jQuery Update module. 

2. Make sure to update your jQuery to version 1.7 or above. You may do
   this by going to admin/config/development/jquery_update and selecting 
   the jQuery version.

3. Download the fancyBox plugin and extract the entire directory into
   sites/all/libraries. (You may need to create this directory). The path to
   fancyBox should be sites/all/libraries/fancybox/source/jquery.fancybox.js.
   (the module uses the minified version, so actually jquery.fancybox.pack.js).

4. Copy the entire fancyBox module's directory into sites/all/modules.

5. Login as an administrator. Enable the module fancyBox and 
   addgalleryclass_filter in admin/modules.

CONFIGURATION
=============

FancyBox module
---------------

Go to admin/config/user-interface/fancybox and fill 
"fancyBox Selectors" (for example: "a:has(img)").


Format Text
-----------

Go to admin/config/content/formats/full_html and 
check "Add attribute for fancyBox gallery".
Or you can create new text format (admin/config/content/formats/add) 
and check this filter there.

USING
=====

Now create node, insert into content some images. Image must be a link to 
itself or some other image. And now you can see all images in 
node through fancyBox gallery.

SUPPORT
=======

Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/...
